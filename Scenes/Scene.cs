﻿using game.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Scenes
{
    public abstract class Scene
    {
        public TileManager _tileManager;
        public string _tileMapPath;
        public ContentManager _content;

        public Scene(ContentManager content, string tileMapPath)
        {
            _tileMapPath = tileMapPath;
            _content = content;
        }
        public virtual void LoadContent()
        {
            _tileManager = new TileManager(_content, _tileMapPath);
            _tileManager.Initialize();
        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            _tileManager.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }
        public virtual void Update(GameTime gameTime)
        {

        }
    }
}
