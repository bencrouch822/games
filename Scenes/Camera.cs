﻿using game.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Scenes
{
    public class Camera
    {
        public Matrix Transform { get; private set; }
        protected float _zoom;
        protected float _scrollValue;

        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; } // Negative zoom will flip image
        }

        private float ZoomMax = 3f;
        private float ZoomMin = 1f;

        public Camera()
        {
            _zoom = 3f;
            _scrollValue = 0;
        }

        // Used by SpriteBatch transform
        public void Follow(Vector2 target, TileManager tileManager)
        {
            var offsetX = (Game1.ScreenWidth / 2) / Zoom;
            var offsetY = (Game1.ScreenHeight / 2) / Zoom;


            // Set initial matrix values
            var position = Matrix.CreateTranslation(
                -target.X,
                -target.Y,
                0);

            var offset = Matrix.CreateTranslation(
                    offsetX,
                    offsetY,
                    0);

            var scale = Matrix.CreateScale(new Vector3(Zoom, Zoom, 1));

            var mapHeight = tileManager._tileMap.GetLength(1) * 16;
            var mapWidth = tileManager._tileMap.GetLength(0) * 16;

            // Makes sure camera doesn't go off the Map on Top and Left
            var targetOffsetX = target.X + offsetX;
            var targetOffsetY = target.Y + offsetY;

            var outLeft = outOfBoundsLeft(target.X, offsetX);
            var outTop = outOfBoundsTop(target.Y, offsetY);
            var outBottom = outOfBoundsBottom(targetOffsetY, mapHeight);
            var outRight = outOfBoundsRight(targetOffsetX, mapWidth);

            // Makes sure camera doesn't go off the Map on Top and Left
            if (outLeft && outTop) // Top Left
            {
                position = Matrix.CreateTranslation(-offsetX, -offsetY, 0);
            }
            else if (outLeft && outBottom) // Bottom Left
            {
                position = Matrix.CreateTranslation(-offsetX, -(mapHeight - offsetY), 0);
            }
            else if (outRight && outTop) // Top Right
            {
                position = Matrix.CreateTranslation(-(mapWidth - offsetX), -offsetY, 0);
            }
            else if (outRight && outBottom) // Bottom Right
            {
                position = Matrix.CreateTranslation(-(mapWidth - offsetX), -(mapHeight - offsetY), 0);
            }
            else
            {

                // Left
                if (outLeft)
                {
                    position = Matrix.CreateTranslation(-offsetX, -target.Y, 0);
                }

                // Top
                if (outTop)
                {
                    position = Matrix.CreateTranslation(-target.X, -offsetY, 0);
                }

                // Right
                if (outRight)
                {
                    position = Matrix.CreateTranslation(-(mapWidth - offsetX), -target.Y, 0);
                }

                // Bottom
                if (outBottom)
                {
                    position = Matrix.CreateTranslation(-target.X, -(mapHeight- offsetY), 0);
                }
            }


            Transform = offset * position * scale;

            HandleZoom();
        }

        private bool outOfBoundsTop(float y, float offsetY)
        {
            return y <= offsetY;
        }

        private bool outOfBoundsLeft(float x, float offsetX)
        {
            return x <= offsetX;
        }

        private bool outOfBoundsBottom(float targetOffsetY, float mapHeight)
        {
            return targetOffsetY >= mapHeight;
        }

        private bool outOfBoundsRight(float targetOffsetX, float mapWidth)
        {
            return targetOffsetX >= mapWidth;
        }

        private void HandleZoom()
        {
            var newScrollValue = Mouse.GetState().ScrollWheelValue;
            var newZoom = 0f;

            if (_scrollValue < newScrollValue)
                newZoom = Zoom + 0.1f;
            else if (_scrollValue > newScrollValue)
                newZoom = Zoom - 0.1f;

            if (Zoom != newZoom && newZoom > ZoomMin && newZoom < ZoomMax)
                Zoom = newZoom;

            _scrollValue = newScrollValue;
        }
    }
}
