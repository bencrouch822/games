﻿using System;
using System.Collections.Generic;
using System.Text;

namespace game.Models
{
    public class TileMapFile
    {
        public List<TileMapLayer> layers;
        public int height;
        public int width;
        public int tileheight;
        public int tilewidth;
    }

    public class TileMapLayer
    {
        public int?[] data;
        public int? height;
        public int? width;
        public List<TileMapObject> Objects;
    }

    public class TileMapObject
    {
        public int Height;
        public int Width;
        public int X;
        public int Y;
    }
}
