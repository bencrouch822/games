﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Models
{
    public class TerrainTile : Tile
    {
        public TerrainTile(Texture2D texture) : base(texture) { }
    }
}
