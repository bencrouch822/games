﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Models
{
    /// <summary>
    /// Flyweight
    /// </summary>
    public abstract class Tile
    {
        public Texture2D _texture;
        public Rectangle _rectangle;

        public Rectangle Rectangle
        {
            get { return _rectangle; }
            set { _rectangle = value; }
        }

        public Tile(Texture2D texture)
        {
            _texture = texture;
            // Rectangle = rectangle;
        }

        public virtual void Draw(SpriteBatch spriteBatch, Rectangle posRectangle)
        {
            spriteBatch.Draw(_texture, posRectangle, Color.White);
        }
    }
}
