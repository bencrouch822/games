﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Models
{
    public class Collision
    {
        public Rectangle Rectangle;

        public Collision(Rectangle rectangle)
        {
            Rectangle = rectangle;
        }
    }
}
