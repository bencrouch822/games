﻿using game.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace game.Sprites
{
    public class Player : Sprite
    {
        private Vector2 Direction;

        public Player(Texture2D texture) : base(texture)
        {
        }

        public Player(Dictionary<string, Animation> animations) : base(animations)
        {
            Speed = 1.5f;
        }

        public override void Update(GameTime gameTime, List<Component> sprites)
        {
            Move();

            SetAnimations();

            _animationManager.Update(gameTime);

            Position += Velocity;
            Velocity = Vector2.Zero;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
                spriteBatch.Draw(_texture, Position, Color.White);
            else if (_animationManager != null)
                _animationManager.Draw(spriteBatch);
            else throw new Exception("This ain't right..!");
        }

        //

        public virtual void Move()
        {
            var keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Input.Right) && keyboardState.IsKeyDown(Input.Down))
                Velocity = new Vector2(Speed, Speed);
            else if (keyboardState.IsKeyDown(Input.Right) && keyboardState.IsKeyDown(Input.Up))
                Velocity = new Vector2(Speed, -Speed);
            else if (keyboardState.IsKeyDown(Input.Left) && keyboardState.IsKeyDown(Input.Down))
                Velocity = new Vector2(-Speed, Speed);
            else if (keyboardState.IsKeyDown(Input.Left) && keyboardState.IsKeyDown(Input.Up))
                Velocity = new Vector2(-Speed, -Speed);
            else if (keyboardState.IsKeyDown(Input.Up))
                Velocity.Y = -Speed;
            else if (keyboardState.IsKeyDown(Input.Down))
                Velocity.Y = Speed;
            else if (keyboardState.IsKeyDown(Input.Left))
                Velocity.X = -Speed;
            else if (keyboardState.IsKeyDown(Input.Right))
                Velocity.X = Speed;

            var newPos = Position + Velocity;
            if (newPos.X <= 0)
            {
                Velocity.X = 0;
            }

            if (newPos.Y <= 0)
            {
                Velocity.Y = 0;
            }
        }

        private float calculateDistance(Vector2 a, Vector2 b)
        {
            a = new Vector2(Math.Abs(a.X), Math.Abs(a.Y));
            b = new Vector2(Math.Abs(b.X), Math.Abs(b.Y));
            var xDiff = a.X - b.X;
            var yDiff = a.Y - b.Y;
            var distance = (float)Math.Sqrt(Math.Pow(xDiff, 2) + Math.Pow(yDiff, 2));

            return Math.Abs(distance);
        }

        protected virtual void SetAnimations()
        {
            if (Velocity.X > 0)
                _animationManager.Play(_animations["WalkingRight"]);
            else if (Velocity.X < 0)
                _animationManager.Play(_animations["WalkingLeft"]);
            else if (Velocity.Y > 0)
                _animationManager.Play(_animations["WalkingDown"]);
            else if (Velocity.Y < 0)
                _animationManager.Play(_animations["WalkingUp"]);
            else _animationManager.Stop();
        }
    }
}
