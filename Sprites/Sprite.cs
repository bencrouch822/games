﻿using game.Managers;
using game.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace game.Sprites
{
    public class Sprite : Component
    {
        #region Fields

        protected AnimationManager _animationManager;

        protected Dictionary<string, Animation> _animations;

        protected Vector2 _position;

        protected Texture2D _texture;
        protected float? _layer { get; set; }

        public float? Layer
        {
            get { return _layer; }
            set
            {
                _layer = value;
            }
        }
        #endregion

        #region Properties

        public Input Input;

        public Rectangle Rectangle;

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;

                if (_animationManager != null)
                    _animationManager.Position = _position;
            }
        }

        public float Speed;

        public Vector2 Velocity;

        #endregion
        public Sprite(Texture2D texture)
        {
            _texture = texture;
            Rectangle = new Rectangle((int)_position.X, (int)_position.Y, _texture.Width, _texture.Height);
        }

        public Sprite(Dictionary<string, Animation> animations)
        {
            _animations = animations;
            _animationManager = new AnimationManager(_animations.First().Value);
        }

        #region Methods

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, Position, Color.White);

                if (Layer != null)
                {
                    spriteBatch.Draw(_texture, Position, null, Color.White, 0, new Vector2(0, 0), 1f, SpriteEffects.None, (float)Layer);
                }
                else
                {
                    spriteBatch.Draw(_texture, Position, Color.White);
                }
            }
        }

        public override void Update(GameTime gameTime, List<Component> components)
        {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
                spriteBatch.Draw(_texture, Position, Color.White);
            else if (_animationManager != null)
                _animationManager.Draw(spriteBatch);
        }
        #endregion
    }
}
