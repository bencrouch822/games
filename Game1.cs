﻿using game.Managers;
using game.Models;
using game.Scenes;
using game.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace game
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private TileManager _tileManager;

        public static int ScreenWidth = 1920;
        public static int ScreenHeight = 1080;

        // REMOVE
        private Player _player;
        private Camera _camera;

        //

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _graphics.PreferredBackBufferWidth = ScreenWidth;
            _graphics.PreferredBackBufferHeight = ScreenHeight;
            _graphics.PreferMultiSampling = false;
            _graphics.GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;
            _graphics.ApplyChanges();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _tileManager = new TileManager(Content);
            _tileManager.Initialize();

            var animation = new Dictionary<string, Animation>()
            {
                { "WalkingUp", new Animation(Content.Load<Texture2D>("Players/WalkingUp"), 3) },
                { "WalkingDown", new Animation(Content.Load<Texture2D>("Players/WalkingDown"), 3) },
                { "WalkingLeft", new Animation(Content.Load<Texture2D>("Players/WalkingLeft"), 3) },
                { "WalkingRight", new Animation(Content.Load<Texture2D>("Players/WalkingRight"), 3) }
            };
            _player = new Player(animation)
            {
                Input = new Input()
                {
                    Up = Keys.W,
                    Down = Keys.S,
                    Left = Keys.A,
                    Right = Keys.D
                }
            };

            _player.Position = new Vector2(
                30,
                30);

            _camera = new Camera();
            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _camera.Follow(_player.Position, _tileManager);
            _player.Update(gameTime, null);
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(transformMatrix: _camera.Transform, samplerState: SamplerState.PointClamp);
            _tileManager.Draw(gameTime, _spriteBatch);
            _player.Draw(gameTime, _spriteBatch);
            _spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
