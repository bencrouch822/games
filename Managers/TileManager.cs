﻿using game.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace game.Managers
{
    public class TileManager
    {
        public Dictionary<int, Tile> _tiles; // Flyweight manager for tile textures
        public int[,] _tileMap; // 2D array with tile map tile IDs
        public Rectangle[] _collisionTiles;

        protected string _tileMapFilePath;
        protected int[] _uniqueTiles;
        protected ContentManager _content;

        public TileManager(ContentManager content, string tileMapFilePath = "C:\\Dev\\games\\second-game\\game\\Content\\Tilemaps\\Beach2.json")
        {
            _tiles = new Dictionary<int, Tile>();
            _tileMapFilePath = tileMapFilePath;
            _content = content;
        }

        /// <summary>
        /// Loads map tiles from input file and populates texture flyweights.
        /// </summary>
        public void Initialize()
        {
            GetTileMapData();
            LoadTileContent();
        }

        /// <summary>
        /// Draws all tiles to map.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            for (var x = 0; x < _tileMap.GetLength(1); x++)
                for (var y = 0; y < _tileMap.GetLength(0); y++)
                    _tiles[_tileMap[y, x]]?.Draw(spriteBatch, new Rectangle(x * 16, y * 16, 16, 16));
        }

        /// <summary>
        /// Returns Tile object if tile ID exists in _tiles.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private Tile GetTile(int key)
        {
            Tile tile = null;

            if (_tiles.ContainsKey(key))
            {
                tile = _tiles[key];
            }

            return tile;
        }

        /// <summary>
        /// Reads JSON file path provided in constructor and populates 2D _tileMap.
        /// </summary>
        private void GetTileMapData()
        {
            try
            {
                using (StreamReader r = new StreamReader(_tileMapFilePath))
                {
                    // Open JSON file containing tile map data
                    string json = r.ReadToEnd();
                    var file = JsonConvert.DeserializeObject<TileMapFile>(json);
                    
                    var uniqueTiles = new List<int>(); // List used for determining unique tile IDs.

                    if (file.layers.Count > 0)
                    {
                        var f = file.layers[0];
                        var d = f.data;
                        int val;
                        
                        _tileMap = new int[(int)f.height, (int)f.width]; // Populate 2D array size based on height and width of tile map.

                        var y = 0;
                        var x = 0;

                        // 'Tiler' program populates tilemap as 1D array.
                        // Need to convert this into a 2D array to get the tile positions correct.
                        for (var i = 0; i < d.Length; i++)
                        {
                            // If iteration is equal to iteration divided by tile map width, we must increment the y index, thus starting the next map row.
                            if (i != 0 && i % f.width == 0)
                            {
                                y = 0;
                                x++;
                            }

                            // Get the tile ID value from our 1D array.
                            // Tiler program adds 1 to each ID
                            _tileMap[x, y] = (int)d[i];
                                
                            if (!uniqueTiles.Contains((int)d[i]))
                            {
                                uniqueTiles.Add((int)d[i]);
                            }
                            
                            y++;
                        }

                        _uniqueTiles = uniqueTiles.ToArray();

                        // Load collision layer objects
                        if (file.layers.Count > 1)
                        {
                            f = file.layers[1];
                            if (f.Objects != null)
                            {
                                var obj = f.Objects;
                                _collisionTiles = new Rectangle[obj.Count];
                                for (var i = 0; i < f.Objects.Count; i++)
                                {
                                    _collisionTiles[i] = new Rectangle(
                                        obj[i].X,
                                        obj[i].Y,
                                        obj[i].Width,
                                        obj[i].Height);
                                } 
                            }
                        }
                    }
                }
            } catch (Exception e)
            {
                Console.WriteLine("Error fetching tilemap data. Error: {}", e.Message);
            }
        }

        /// <summary>
        /// Populates Tile flyweight objects with Texture2D corresponding to tile ID from map.
        /// TODO: Load file content from a Map class.
        /// </summary>
        private void LoadTileContent()
        {
            string textureName;

            foreach (var tileItem in _uniqueTiles)
            {

                // tileItem will equal the tile ID provided by the map tile array
                switch (tileItem)
                {
                    case 2: // Sand
                        textureName = "Terrain/Sand_Dry";
                        break;
                    case 3:
                        textureName = "Terrain/Sand_Wet";
                        break;
                    case 5:
                        textureName = "Terrain/Sand_Wood";
                        break;
                    case 21:
                        textureName = "Terrain/Sand_Rocks";
                        break;
                    case 51:
                        textureName = "Terrain/Ocean_Shallow";
                        break;
                    default:
                        textureName = null;
                        break;
                }

                // Adds Tile to _tiles dictionary if it doesn't already exist.
                if (textureName != null)
                {
                    var texture = _content.Load<Texture2D>(textureName);
                    var tile = new TerrainTile(texture);
                    _tiles.Add(tileItem, tile);
                }
            }
        }
    }
}
